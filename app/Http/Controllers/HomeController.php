<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Fortify\Contracts\RegisterResponse;
use Laravel\Fortify\Contracts\RegisterViewResponse;
use Laravel\Socialite\Facades\Socialite;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dashboard(Request $request)
    {
        $user = auth()->user();
        $users = User::where('id', '<>',$user->id);
        $income = explode(',',$user->expected_income);
        if(isset($request->expected_income_from)){
            $users = $users->where('annual_income','>=',$request->expected_income_from);
        }elseif(isset($income[0])){
            $request->expected_income_from= $income[0];
            $users = $users->where('annual_income','>=',$income[0]);
        }
        if(isset($request->expected_income_to)){
            $users = $users->where('annual_income','<=',$request->expected_income_to);
        }elseif(isset($income[1])){
            $request->expected_income_to= $income[1];
            $users = $users->where('annual_income','<=',$income[1]);
        }
        if(isset($request->partner_occupation)){
            $users = $users->whereIn('occupation',explode(',',$request->partner_occupation));
        }elseif(isset($user->partner_occupation)){
            $request->partner_occupation=$user->partner_occupation;
            $users = $users->whereIn('occupation',explode(',',$user->partner_occupation));
        }
        if(isset($request->partner_family_type)){
            $users = $users->whereIn('family_type',explode(',',$request->partner_family_type));
        }elseif(isset($user->partner_family_type)){
            $request->partner_family_type=$user->partner_family_type;
            $users = $users->whereIn('family_type',explode(',',$user->partner_family_type));
        }
        if(isset($request->partner_manglik)){
            $users = $users->whereIn('manglik',explode(',',$request->partner_manglik));
        }elseif(isset($user->partner_manglik)){
            $request->partner_manglik=$user->partner_manglik;
            $users = $users->whereIn('manglik',explode(',',$user->partner_manglik));
        }
        $users = $users->paginate();
        return view('dashboard',compact('users'));
    }

    /**
     * Handle Social login request
     * @param $social
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }



    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user();
        $user = User::where(['email' => $userSocial->getEmail()]);
        if($social == 'facebook_id'){
            $user = $user->orWhere('facebook_id', $userSocial->getId());
        }else{
            $user = $user->orWhere('google_id', $userSocial->getId());
        }
        $user = $user->first();
        if($user){
            if(!isset($user->google_id)){
                $user->google_id = $userSocial->getId();
                $user->save();
            }
            auth()->login($user->fresh());
            return redirect()->to('/dashboard');
        }else{
            return redirect()->route('register');
        }
    }
}
