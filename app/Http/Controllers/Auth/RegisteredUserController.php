<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Contracts\RegisterResponse;
use Laravel\Fortify\Contracts\RegisterViewResponse;

class RegisteredUserController extends Controller
{
    /**
     * The guard implementation.
     *
     * @var StatefulGuard
     */
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param StatefulGuard
     * @return void
     */
    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * Show the registration view.
     *
     * @param Request $request
     * @return RegisterViewResponse
     */
    public function create(Request $request): RegisterViewResponse
    {
        return app(RegisterViewResponse::class);
    }

    /**
     * Create a new registered user.
     *
     * @param Request $request
     * @param User $creator
     * @return RegisterResponse
     */
    public function store(Request $request,
                          User $creator): RegisterResponse
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'date_of_birth' => 'required|date',
            'gender' => 'required',
            'annual_income' => 'required',
            'password' => 'required|confirmed',
        ]);
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['expected_income'] = implode(',', $request->all(['expected_income_from', 'expected_income_to']));
        unset($data['expected_income_from']);
        unset($data['expected_income_to']);
        $data['date_of_birth'] = date('Y-m-d', strtotime($request->date_of_birth));
        event(new Registered($user = $creator->create($data)));

        $this->guard->login($user);

        return app(RegisterResponse::class);
    }
}
