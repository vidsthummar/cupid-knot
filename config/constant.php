<?php

return [
    "occupation" => [0 => 'Private job', 'Government Job', 'Business'],
    "family_type" => [0 => 'Joint family', 'Nuclear family'],
    "manglik" => [0 => 'Yes', 'No'],
];
