<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login/{social}',[\App\Http\Controllers\HomeController::class,'socialLogin'])->where('social','google');
Route::get('/login/{social}/callback',[\App\Http\Controllers\HomeController::class,'handleProviderCallback'])->where('social','google');

Route::get('/register', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'create'])
    ->middleware(['guest'])
    ->name('register');

Route::post('/register', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'store'])
    ->middleware(['guest']);


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [\App\Http\Controllers\HomeController::class,'dashboard'])->name('dashboard');
