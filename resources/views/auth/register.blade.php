<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <h1 class="text-4xl">Basic Information Section</h1>
            <hr class="pb-2">
            <div class="grid gap-4 grid-cols-2">
                <div class="">
                    <x-jet-label for="first_name" value="{{ __('First Name') }}" />
                    <x-jet-input id="first_name" class="block mt-1 w-full" type="text" name="first_name" :value="old('first_name')" required autofocus autocomplete="first_name" />
                </div>
                <div class="">
                    <x-jet-label for="last_name" value="{{ __('Last Name') }}" />
                    <x-jet-input id="last_name" class="block mt-1 w-full" type="text" name="last_name" :value="old('last_name')" required autofocus autocomplete="last_name" />
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="grid gap-4 grid-cols-2 mt-4">
                <div class="">
                    <x-jet-label for="first_name" value="{{ __('Date of Birth') }}" />
                    <x-jet-datepicker id="date_of_birth" class="block mt-1 w-full" name="date_of_birth" placeholder="Date of Birth" :value="old('date_of_birth')" required></x-jet-datepicker>
                </div>
                <div class="">
                    <x-jet-label for="email" value="{{ __('Gender') }}" />
                    <div class="grid-cols-2">
                        <label class="inline-flex items-center mt-3">
                            <x-jet-input class="form-radio rounded-xs h-6 w-5 text-gray-600" type="radio" name="gender" value="0" required />
                            <span class="ml-2 text-gray-700">Male</span>
                        </label>
                        <label class="inline-flex items-center mt-3">
                            <x-jet-input class="form-radio rounded-xs h-6 w-5 text-gray-600" type="radio" name="gender" value="1" required />
                            <span class="ml-2 text-gray-700">Female</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="grid gap-4 grid-cols-2 mt-4">
                <div class="">
                    <x-jet-label for="annual_income" value="{{ __('Annual income') }}" />
                    <x-jet-input id="annual_income" class="block mt-1 w-full" type="number" name="annual_income" :value="old('annual_income')" required autofocus autocomplete="annual_income" />
                </div>
                <div class="">
                    @php
                    $data = [
                            'data'=>config('constant.occupation'),
                            'emptyOptionsMessage' =>  'No occupation match your search.',
                            'name' => 'occupation',
                            'placeholder' => 'Select a Occupation',
                            'value' => old('occupation'),
                        ];
                    @endphp
                    <x-jet-label for="occupation" value="{{ __('Occupation') }}" />
                    <x-jet-select name="occupation" data="{{ json_encode($data) }}" />
                </div>
            </div>

            <div class="grid gap-4 grid-cols-2 mt-4">
                <div class="">
                    @php
                        $data = [
                                'data'=>[
                                        0 => 'Joint family',
                                        1 => 'Nuclear family'
                                    ],
                                'emptyOptionsMessage' =>  'No family type match your search.',
                                'name' => 'family_type',
                                'placeholder' => 'Select a Family Type',
                                'value' => old('family_type'),
                            ];
                    @endphp
                    <x-jet-label for="family_type" value="{{ __('Family Type') }}" />
                    <x-jet-select name="family_type" data="{{ json_encode($data) }}" />
                </div>
                <div class="">
                    @php
                        $data = [
                                'data'=>[
                                        0 => 'Yes',
                                        1 => 'No'
                                    ],
                                'emptyOptionsMessage' =>  'No manglik match your search.',
                                'name' => 'manglik',
                                'placeholder' => 'Are you Manglik?',
                                'value' => old('manglik'),
                            ];
                    @endphp
                    <x-jet-label for="manglik" value="{{ __('Manglik') }}" />
                    <x-jet-select name="manglik" data="{{ json_encode($data) }}" />
                </div>
            </div>

            <div class="grid gap-4 grid-cols-2">
                <div class="mt-4">
                    <x-jet-label for="password" value="{{ __('Password') }}" />
                    <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                    <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                </div>
            </div>
            <h1 class="text-4xl pt-5">Partner Preference</h1>
            <hr class="pb-2">
            <div class="grid gap-4 grid-cols-2 mt-4">
                <div class="">
                    <x-jet-label for="expected_income" class="pb-3" value="{{ __('Expected Income') }}" />
                    <x-jet-range-slider name="expected_income"  old_min="{!! old('expected_income_from',15000) !!}" old_max="{!! old('expected_income_to',150000) !!}" />
                </div>
                <div class="">
                    @php
                        $data = [
                                'data'=>[
                                        0 => 'Private job',
                                        1 => 'Government Job',
                                        2 => 'Business',
                                    ],
                                'emptyOptionsMessage' =>  'No occupation match your search.',
                                'name' => 'partner_occupation',
                                'placeholder' => 'Select a occupation',
                            ];
                    @endphp
                    <x-jet-label for="partner_occupation" value="{{ __('Occupation') }}" />
                    <x-jet-multi-select name="partner_occupation" id="partner_occupation" data="{{ json_encode($data) }}" />
                </div>
            </div>
            <div class="grid gap-4 grid-cols-2 mt-4">
                <div class="">
                    @php
                        $data = [
                                'data'=>[
                                        0 => 'Joint family',
                                        1 => 'Nuclear family'
                                    ],
                                'emptyOptionsMessage' =>  'No family type match your search.',
                                'name' => 'partner_family_type',
                                'placeholder' => 'Select a family type',
                            ];
                    @endphp
                    <x-jet-label for="partner_family_type" value="{{ __('Family Type') }}" />
                    <x-jet-multi-select name="partner_family_type" id="partner_family_type" data="{{ json_encode($data) }}" />
                </div>
                <div class="">
                    @php
                        $data = [
                                'data'=>[
                                        0 => 'Yes',
                                        1 => 'No'
                                    ],
                                'emptyOptionsMessage' =>  'No option match your search.',
                                'name' => 'partner_manglik',
                                'placeholder' => 'Manglik',
                                'value' => old('partner_manglik'),
                            ];
                    @endphp
                    <x-jet-label for="partner_manglik" value="{{ __('Manglik') }}" />
                    <x-jet-multi-select name="partner_manglik" id="partner_manglik" data="{{ json_encode($data) }}" />
                </div>
            </div>
            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
