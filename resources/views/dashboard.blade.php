<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="">
                {{--                <x-jet-welcome />--}}
                <div class="flex">
                    <div class="w-1/4 p-4 mr-2 bg-white shadow-xl sm:rounded-lg">
                        <form>
                            <div class="">
                                <x-jet-label for="expected_income" class="pb-3" value="{{ __('Expected Income') }}"/>
                                <x-jet-range-slider name="expected_income" old_min="{!! request()->expected_income_from !!}" old_max="{!! request()->expected_income_to !!}" />
                            </div>
                            <div class="">
                                @php
                                    $data = [
                                            'data'=>[
                                                    0 => 'Private job',
                                                    1 => 'Government Job',
                                                    2 => 'Business',
                                                ],
                                            'emptyOptionsMessage' =>  'No occupation match your search.',
                                            'name' => 'partner_occupation',
                                            'placeholder' => 'Select a occupation',
                                            'value' => request()->partner_occupation
                                        ]
                                @endphp
                                <x-jet-label for="partner_occupation" value="{{ __('Occupation') }}"/>
                                <x-jet-multi-select name="partner_occupation" id="partner_occupation"
                                                    data="{{ json_encode($data) }}"/>
                            </div>
                            <div class="">
                                @php
                                    $data = [
                                            'data'=>[
                                                    0 => 'Joint family',
                                                    1 => 'Nuclear family'
                                                ],
                                            'emptyOptionsMessage' =>  'No family type match your search.',
                                            'name' => 'partner_family_type',
                                            'placeholder' => 'Select a family type',
                                            'value' => request()->partner_family_type
                                        ]
                                @endphp
                                <x-jet-label for="partner_family_type" value="{{ __('Family Type') }}"/>
                                <x-jet-multi-select name="partner_family_type" id="partner_family_type"
                                                    data="{{ json_encode($data) }}"/>
                            </div>
                            <div class="">
                                @php
                                    $data = [
                                            'data'=>[
                                                    0 => 'Yes',
                                                    1 => 'No'
                                                ],
                                            'emptyOptionsMessage' =>  'No option match your search.',
                                            'name' => 'partner_manglik',
                                            'placeholder' => 'Manglik',
                                            'value' => request()->partner_manglik
                                        ]
                                @endphp
                                <x-jet-label for="partner_manglik" value="{{ __('Manglik') }}"/>
                                <x-jet-multi-select name="partner_manglik" id="partner_manglik"
                                                    data="{{ json_encode($data) }}"/>
                            </div>
                            <div class="">
                                <button
                                    type="submit"
                                    class="border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
                                >
                                    Primary
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="w-3/4 py-2 ml-2 bg-white shadow-xl sm:rounded-lg">
                        @if (count($users->items()) > 0)
                            @foreach($users->items() as $user)
                                <div class="p-4 my-2 mx-4 grid col-cols-2 gap-4 text-white bg-purple-600 rounded-lg shadow-xs">
                                    <h4 class="col-span-2">
                                        <b>{!! strtoupper($user->first_name).' '.strtoupper($user->last_name) !!}</b>
                                    </h4>

                                    <div class="">
                                        <b>Date of Birth:</b> {!! date('d/m/Y', strtotime($user->date_of_birth)) !!}
                                    </div>
                                    <div class="">
                                        <b>Gender:</b> {!! $user->gender === 0 ? 'Male' : 'Female' !!}
                                    </div>
                                    <div class="">
                                        <b>Annual Income:</b> ₹ {!! $user->annual_income !!}
                                    </div>
                                    <div class="">
                                        <b>Occupation:</b> {!! config("constant.occupation." . $user->occupation) !!}
                                    </div>
                                    <div class="">
                                        <b>Family Type:</b> {!! config("constant.family_type." . $user->family_type) !!}
                                    </div>
                                    <div class="">
                                        <b>Manglik:</b> {!! config("constant.manglik." . $user->manglik) !!}
                                    </div>
                                </div>
                            @endforeach
                            @if ($users->hasPages())
                                    <div class="min-w-0 my-2 mx-4 p-4 text-white bg-purple-300 rounded-lg shadow-xs">
                                        {{ $users->links() }}
                                    </div>
                            @endif
                        @else
                            <div class="min-w-0 my-2  mx-4 text-4xl p-8 text-center text-white bg-purple-600 rounded-lg shadow-xs">
                                No Match Found
                            </div>
                        @endif

                        {{--<div class="min-w-0 my-2 mx-4 p-4 text-white bg-purple-600 rounded-lg shadow-xs">
                            <h4 class="mb-4 font-semibold">
                                Colored card
                            </h4>
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                Fuga, cum commodi a omnis numquam quod? Totam exercitationem
                                quos hic ipsam at qui cum numquam, sed amet ratione! Ratione,
                                nihil dolorum.
                            </p>
                        </div>
                        <div class="min-w-0 my-2 mx-4 p-4 text-white bg-purple-600 rounded-lg shadow-xs">
                            <h4 class="mb-4 font-semibold">
                                Colored card
                            </h4>
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                Fuga, cum commodi a omnis numquam quod? Totam exercitationem
                                quos hic ipsam at qui cum numquam, sed amet ratione! Ratione,
                                nihil dolorum.
                            </p>
                        </div>
                        <div class="min-w-0 my-2 mx-4 p-4 text-white bg-purple-600 rounded-lg shadow-xs">
                            <h4 class="mb-4 font-semibold">
                                Colored card
                            </h4>
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                Fuga, cum commodi a omnis numquam quod? Totam exercitationem
                                quos hic ipsam at qui cum numquam, sed amet ratione! Ratione,
                                nihil dolorum.
                            </p>
                        </div>
                        <div class="min-w-0 my-2 mx-4 p-4 text-white bg-purple-600 rounded-lg shadow-xs">
                            <h4 class="mb-4 font-semibold">
                                Colored card
                            </h4>
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                Fuga, cum commodi a omnis numquam quod? Totam exercitationem
                                quos hic ipsam at qui cum numquam, sed amet ratione! Ratione,
                                nihil dolorum.
                            </p>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
</x-app-layout>
