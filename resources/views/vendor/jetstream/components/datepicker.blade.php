@props(['disabled' => false])
<input type="text" {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'form-input rounded-md shadow-sm']) !!}>
<script>
    $({!! $attributes->get('id') !!}).datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: "-18Y"
    });
</script>
