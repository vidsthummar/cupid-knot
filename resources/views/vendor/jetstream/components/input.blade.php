@props(['disabled' => false])

<input {{ $attributes->get('type')==='radio' && old($attributes->get('name'))== $attributes->get('value') ? 'checked' :'' }} {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'form-input rounded-md shadow-sm']) !!}>
