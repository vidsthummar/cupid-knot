<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
    <div>
        {{ $logo }}
    </div>
    @if (request()->getRequestUri() === '/register')
        <div class="w-full xl:max-w-5xl mb-10 mt-6 px-6 py-10 bg-white shadow-md overflow-hidden sm:rounded-lg">
    @else
        <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    @endif
        {{ $slot }}
    </div>
</div>
